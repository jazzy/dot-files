set history save on
set print pretty on

python
import sys
sys.path.insert(1, '/home/jsilva/Boost-Pretty-Printer')
import boost
boost.register_printers(boost_version=(1,65,1))
end
