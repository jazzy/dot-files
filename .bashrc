#######################################################
# .bashrc file do Joao Silva
# www.bashscripts.org
#
# Last Modified 12-11-2006
# Running on Red Hat Enterprise Linux ES release 4 (Nahant)
#######################################################


# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# enable programmable completion features
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi


# Sources colors
if [ -f ~/.bash_colors ]; then
    . ~/.bash_colors
fi

# Git related alias
if [ -f ~/.bash_git ]; then
    . ~/.bash_git
fi

# Aliases to local workstations
if [ -f ~/.myhosts ]; then
    . ~/.myhosts
fi

if [ -f ~/.functions ]; then
	. ~/.functions
fi

if [ -f ~/.git-prompt.sh ]; then
	. ~/.git-prompt.sh
fi

if [ -f ~/.gitps1 ]; then
	. ~/.gitps1
fi

# EXPORTS
#######################################################

# User specific environment and startup programs
export EDITOR=vi
PATH=$PATH:$HOME/bin

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/externalLibs/lib

export PATH
unset USERNAME

export PS1="[\[\033[1;34m\w\[\033[0m] \[$MAGENTA\]\$(__git_ps1)\[$WHITE\] \n[\t \u@\h]$ "
#export PS1="\t\n[\u@\h \w]$ "
export HISTFILESIZE=10000 # the bash history should save 3000 commands
export HISTSIZE=10000
export HISTCONTROL=ignoredups #don't put duplicate lines in the history.
export HISTTIMEFORMAT="%d/%m/%y %T "
alias hist='history | grep $1' #Requires one input

# User specific aliases and functions

alias ps='ps auxf'
alias home='cd ~'
alias pg='ps aux | grep'  #requires an argument
alias un='tar -zxvf'
alias mountedinfo='df -hT'
#alias ping='ping -c 10'
alias openports='netstat -nape --inet'
alias ns='netstat -alnp --protocol=inet | grep -v CLOSE_WAIT | cut -c-6,21-94 | tail +2'
alias du1='du -h --max-depth=1'
alias da='date "+%Y-%m-%d %A    %T %Z"'

# Alias to multiple ls commands
alias ls='ls --color=auto'
alias la='ls -Al'               # show hidden files
alias ll='ls -alF'
#alias ls='ls -aF --color=always' # add colors and file type extensions
#alias ls='ls -aF --si' # add file type extensions and human readable sizes
alias lx='ls -lXB'              # sort by extension
alias lk='ls -lSr'              # sort by size
alias lc='ls -lcr'      # sort by change time
alias lu='ls -lur'      # sort by access time
alias lr='ls -lR'               # recursive ls
alias lt='ls -ltr'              # sort by date
alias lm='ls -al |less'         # pipe through 'less'

#Full autotools configuration cycle
alias reconf='aclocal; autoheader; automake -a; ./configure'

#para gerar core files
ulimit -c unlimited
ulimit -H -c unlimited

